
return function(gate)
	if #gate.queueBits~=0 then
		local bit = table.remove(gate.queueBits, #gate.queueBits)
		gate.ports[1]:setstate(bit)
		gate:queue(1)
	else
		gate.ports[1]:setstate(false)
	end
end
