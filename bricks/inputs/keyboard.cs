
datablock fxDTSBrickData(LogicGate_Keyboard_Data){
	brickFile = $LuaLogic::Path @ "bricks/blb/keyboardgate.blb";
	category = "Logic Bricks";
	subCategory = "Inputs";
	uiName = "Input Keyboard";
	iconName = $LuaLogic::Path @ "icons/Input Keyboard";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = true;
	
	logicUIName = "Input Keyboard";
	logicUIDesc = "";
	
	logicInit   = lualogic_readfile($LuaLogic::Path @ "bricks/inputs/keyboard-init.lua"  );
	logicInput  = lualogic_readfile($LuaLogic::Path @ "bricks/inputs/keyboard-input.lua" );
	logicUpdate = lualogic_readfile($LuaLogic::Path @ "bricks/inputs/keyboard-update.lua");
	logicGlobal = lualogic_readfile($LuaLogic::Path @ "bricks/inputs/keyboard-global.lua");
	
	numLogicPorts = 1;
	
	logicPortType[0] = 0;
	logicPortPos[0] = "2 0 0";
	logicPortDir[0] = 1;
	logicPortUIName[0] = "R";

};
lualogic_registergatedefinition("LogicGate_Keyboard_Data");

function LogicGate_Keyboard_Data::LuaLogic_PressKey(%data, %brick, %key, %state){
	%key = strReplace(%key, ";", "\\:");
	lualogic_sendInput(%brick, 2, %key, %state);
}

function serverCmdLInputKey(%client, %key, %state){
	%brick = %client.Logic_InputKeyboard;
	if(isObject(%brick) && %client.Logic_InputActive){
		%brick.getDatablock().LuaLogic_PressKey(%brick, %key, %state);
	}
}

function LogicGate_Keyboard_Data::Logic_OnInput(%data, %brick, %pos, %norm, %client){
	if(isObject(%client)){
		%client.Logic_InputKeyboard = %brick;
		%client.Logic_InputActive = true;
		commandToClient(%client, 'LStartInput');
	}
}
