
local function getBit(gate, val)
	--print("get bit", val)
	if gate.listenState=="wait" then
		if val==1 then
			gate.listenState = "getbits"
			gate.bitsReceived = 0
			--print("", "state = listen")
		end
	elseif gate.listenState=="getbits" then
		gate.valReceived = gate.valReceived + math.pow(2, textbrick2_bitsNeeded-1-gate.bitsReceived)*val
		gate.bitsReceived = gate.bitsReceived+1
		
		--print("", "append", gate.bitsReceived-1, val)
		
		if gate.bitsReceived==textbrick2_bitsNeeded then
			
			gate.listenState = "terminate"
		end
	elseif gate.listenState=="terminate" then
		if val==1 then
			--print("", "terminate")
			
			local printid = gate.valReceived
			local printname = textbrick2_idxToPrint[printid]
			
			gate:cb(printname)
			
			--print("", "set print", string.format("%02x", printid), printname)
		end
		
		gate.listenState = "wait"
		
		gate.bitsReceived = 0
		gate.valReceived = 0
	end
end

local function changeTo(gate, val)
	local tick = gate:gettick()
	local ticks = tick-gate.lastTickChanged
	
	local bits = math.min(ticks, 10)
	for i = 1, bits do
		getBit(gate, val)
	end
	
	gate.lastTickChanged = tick
end

return function(gate)
	if gate.ports[1]:isrising() then
		changeTo(gate, 0)
	elseif gate.ports[1]:isfalling() then
		changeTo(gate, 1)
	end
end
