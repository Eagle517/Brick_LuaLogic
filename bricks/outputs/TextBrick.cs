
datablock fxDTSBrickData(LogicGate_TextBrick_Data){
	brickFile = $LuaLogic::Path @ "bricks/blb/TextBrick.blb";
	category = "Logic Bricks";
	subCategory = "Outputs";
	uiName = "Text Brick";
	iconName = $LuaLogic::Path @ "icons/Text Brick";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;

	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;

	logicUIName = "Text Brick";
	logicUIDesc = "Resets on rise, increments based on pulse length";
	
	logicInit   = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/text-init.lua"  );
	logicUpdate = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/text-update.lua");

	numLogicPorts = 1;

	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -1";
	logicPortDir[0] = 3;
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "Inc";
};
lualogic_registergatedefinition("LogicGate_TextBrick_Data");

function LogicGate_TextBrick_Data::LuaLogic_Callback(%data, %brick, %printname){
	%brick.setPrint(lualogic_getprint(%printname));
}
