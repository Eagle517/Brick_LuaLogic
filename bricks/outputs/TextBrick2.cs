
datablock fxDTSBrickData(LogicGate_TextBrick2_Data){
	brickFile = $LuaLogic::Path @ "bricks/blb/TextBrick.blb";
	category = "Logic Bricks";
	subCategory = "Outputs";
	uiName = "Text Brick 2";
	iconName = $LuaLogic::Path @ "icons/Text Brick";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;

	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;

	logicUIName = "Text Brick 2";
	logicUIDesc = "Takes 7-bit serial input for ascii character";
	
	logicInit   = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/text2-init.lua"  );
	logicUpdate = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/text2-update.lua");
	logicGlobal = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/text2-global.lua");

	numLogicPorts = 1;

	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -1";
	logicPortDir[0] = 3;
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "In";
};
lualogic_registergatedefinition("LogicGate_TextBrick2_Data");

function LogicGate_TextBrick2_Data::LuaLogic_Callback(%data, %brick, %printname){
	%brick.setPrint(lualogic_getprint(%printname));
}
