datablock fxDTSBrickData(LogicGate_Pixel_Data)
{
	brickFile = $LuaLogic::Path @ "bricks/blb/pixels/pixel.blb";
	category = "Logic Bricks";
	subCategory = "Outputs";
	uiName = "Pixel";
	iconName = $LuaLogic::Path @ "icons/Pixel";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;

	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;

	logicUIName = "Pixel";
	logicUIDesc = "";

	logicUpdate = "return function(gate) gate:cb(bool_to_int[gate.ports[1].state] .. bool_to_int[gate.ports[2].state] .. bool_to_int[gate.ports[3].state]) end";

	numLogicPorts = 3;

	logicPortType[0] = 1;
	logicPortPos[0] = "-1 0 -4";
	logicPortDir[0] = 3;
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "R";

	logicPortType[1] = 1;
	logicPortPos[1] = "-1 0 0";
	logicPortDir[1] = 3;
	logicPortCauseUpdate[1] = true;
	logicPortUIName[1] = "G";

	logicPortType[2] = 1;
	logicPortPos[2] = "-1 0 4";
	logicPortDir[2] = 3;
	logicPortCauseUpdate[2] = true;
	logicPortUIName[2] = "B";
};
lualogic_registergatedefinition("LogicGate_Pixel_Data");

function LogicGate_Pixel_Data::LuaLogic_Callback(%this, %brick, %color){
	if(lualogic_isprint("COLOR" @ %color))
		%brick.setPrint(lualogic_getprint("COLOR" @ %color));
}

datablock fxDTSBrickData(LogicGate_HorizontalPixel_Data : LogicGate_Pixel_Data)
{
	brickFile = $LuaLogic::Path @ "bricks/blb/pixels/HPixel.blb";
	category = "Logic Bricks";
	subCategory = "Outputs";
	uiName = "Horizontal Pixel";
	iconName = $LuaLogic::Path @ "icons/Horizontal Pixel";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;

	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;

	logicUIName = "Horizontal Pixel";
	logicUIDesc = "";

	numLogicPorts = 3;

	logicPortType[0] = 1;
	logicPortPos[0] = "-1 1 0";
	logicPortDir[0] = 5;
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "R";

	logicPortType[1] = 1;
	logicPortPos[1] = "-1 -1 0";
	logicPortDir[1] = 5;
	logicPortCauseUpdate[1] = true;
	logicPortUIName[1] = "G";

	logicPortType[2] = 1;
	logicPortPos[2] = "1 -1 0";
	logicPortDir[2] = 5;
	logicPortCauseUpdate[2] = true;
	logicPortUIName[2] = "B";
};
lualogic_registergatedefinition("LogicGate_HorizontalPixel_Data");

function LogicGate_HorizontalPixel_Data::LuaLogic_Callback(%this, %obj, %data){
	LogicGate_Pixel_Data::LuaLogic_Callback(%this, %obj, %data);
}

datablock fxDTSBrickData(LogicGate_SmallPixel_Data){
	brickFile = $LuaLogic::Path @ "bricks/blb/TextBrick.blb";
	category = "Logic Bricks";
	subCategory = "Outputs";
	uiName = "Small Pixel";
	iconName = $LuaLogic::Path @ "icons/Text Brick";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicUIName = "Small pixel";
	logicUIDesc = "Resets on rise, increments based on pulse length";
	
	logicInit   = "return function(gate) gate.tickStarted = 0 end";
	logicUpdate = lualogic_readfile($LuaLogic::Path @ "bricks/outputs/pixel-update.lua");
	
	numLogicPorts = 1;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 0 -1";
	logicPortDir[0] = 3;
	logicPortCauseUpdate[0] = true;
	logicPortUIName[0] = "Inc";
};
lualogic_registergatedefinition("LogicGate_SmallPixel_Data");

function LogicGate_SmallPixel_Data::LuaLogic_Callback(%data, %brick, %color){
	LogicGate_Pixel_Data::LuaLogic_Callback(%data, %brick, %color);
}
