
lualogic_require($LuaLogic::Path @ "bricks/memory/DFlipFlop.cs");

datablock fxDTSBrickData(LogicGate_SRLatch_Data : LogicGate_DFlipFlop_Data)
{
	subCategory = "Memory";
	uiName = "SR Latch";
	iconName = $LuaLogic::Path @ "icons/D FlipFlop";
	
	logicUIName = "SR Latch";
	logicUIDesc = "Q becomes 0 if R is on, else 1 when S is on";
	
	logicUpdate = "return function(gate) if gate.ports[1].state then gate.ports[3]:setstate(false) elseif gate.ports[2].state then gate.ports[3]:setstate(true) end end";
	
	logicPortUIName[0] = "R";
	logicPortCauseUpdate[0] = true;
	logicPortUIName[1] = "S";
	logicPortCauseUpdate[1] = true;
	logicPortUIName[2] = "Q";
};
lualogic_registergatedefinition("LogicGate_SRLatch_Data");
