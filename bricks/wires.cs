datablock fxDTSBrickData(LogicWire1x1fData : brick1x1fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x1f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x2fData : brick1x2fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x2f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x2f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x3fData : brick1x3fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x3f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x3f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x4fData : brick1x4fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x4f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x4f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x5fData : LogicWire1x4fData)
{
	uiName = "Wire 1x5f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x5f";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x5f.blb";
};

datablock fxDTSBrickData(LogicWire1x6fData : brick1x6fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x6f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x6f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x7fData : LogicWire1x5fData)
{
	uiName = "Wire 1x7f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x7f";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x7f.blb";
};

datablock fxDTSBrickData(LogicWire1x8fData : brick1x8fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x8f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x8f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x9fData : LogicWire1x5fData)
{
	uiName = "Wire 1x9f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x9f";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x9f.blb";
};

datablock fxDTSBrickData(LogicWire1x10fData : brick1x10fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x10f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x10f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x11fData : LogicWire1x5fData)
{
	uiName = "Wire 1x11f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x11f";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x11f.blb";
};

datablock fxDTSBrickData(LogicWire1x12fData : brick1x12fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x12f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x12f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x13fData : LogicWire1x5fData)
{
	uiName = "Wire 1x13f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x13f";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x13f.blb";
};

datablock fxDTSBrickData(LogicWire1x14fData : LogicWire1x5fData)
{
	uiName = "Wire 1x14f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x14f";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x14f.blb";
};

datablock fxDTSBrickData(LogicWire1x15fData : LogicWire1x5fData)
{
	uiName = "Wire 1x15f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x15f";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x15f.blb";
};

datablock fxDTSBrickData(LogicWire1x16fData : brick1x16fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x16f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x16f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x32fData : LogicWire1x5fData)
{
	uiName = "Wire 1x32f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x32f";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x32f.blb";
};

datablock fxDTSBrickData(LogicWire1x64fData : LogicWire1x5fData)
{
	uiName = "Wire 1x64f";
	iconName = $LuaLogic::Path @ "icons/Wire 1x64f";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x64f.blb";
};

datablock fxDTSBrickData(LogicWire1x1x2Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x2";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x2";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x2.blb";
};

datablock fxDTSBrickData(LogicWire1x1x3Data : brick1x1Data)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x1x3";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x3";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x1x4Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x4";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x4";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x4.blb";
};

datablock fxDTSBrickData(LogicWire1x1x5Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x5";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x5";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x5.blb";
};

datablock fxDTSBrickData(LogicWire1x1x6Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x6";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x6";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x6.blb";
};

datablock fxDTSBrickData(LogicWire1x1x7Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x7";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x7";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x7.blb";
};

datablock fxDTSBrickData(LogicWire1x1x8Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x8";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x8";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x8.blb";
};

datablock fxDTSBrickData(LogicWire1x1x9Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x9";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x9";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x9.blb";
};

datablock fxDTSBrickData(LogicWire1x1x10Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x10";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x10";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x10.blb";
};

datablock fxDTSBrickData(LogicWire1x1x11Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x11";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x11";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x11.blb";
};

datablock fxDTSBrickData(LogicWire1x1x12Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x12";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x12";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x12.blb";
};

datablock fxDTSBrickData(LogicWire1x1x13Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x13";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x13";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x13.blb";
};

datablock fxDTSBrickData(LogicWire1x1x14Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x14";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x14";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x14.blb";
};

datablock fxDTSBrickData(LogicWire1x1x15Data : Brick1x1x5Data)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 1x1x15";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x15";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire1x1x16Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x16";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x16";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x16.blb";
};

datablock fxDTSBrickData(LogicWire1x1x17Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x17";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x17";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x17.blb";
};

datablock fxDTSBrickData(LogicWire1x1x18Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x18";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x18";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x18.blb";
};

datablock fxDTSBrickData(LogicWire1x1x24Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x24";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x24";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x24.blb";
};

datablock fxDTSBrickData(LogicWire1x1x32Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x32";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x32";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x32.blb";
};

datablock fxDTSBrickData(LogicWire1x1x36Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x36";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x36";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x36.blb";
};

datablock fxDTSBrickData(LogicWire1x1x64Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x64";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x64";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x64.blb";
};

datablock fxDTSBrickData(LogicWire1x1x72Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x72";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x72";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x72.blb";
};

datablock fxDTSBrickData(LogicWire1x1x128Data : LogicWire1x64fData)
{
	uiName = "Wire 1x1x128";
	iconName = $LuaLogic::Path @ "icons/Wire 1x1x128";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x1x128.blb";
};

datablock fxDTSBrickData(LogicWire1x2x5Data : LogicWire1x64fData)
{
	uiName = "Wire 1x2x5";
	iconName = $LuaLogic::Path @ "icons/Wire 1x2x5";
	brickFile = $LuaLogic::Path @ "bricks/blb/wires/1x2x5.blb";
};

datablock fxDTSBrickData(LogicWire64x64fData : brick64x64fData)
{
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 64x64f";
	iconName = $LuaLogic::Path @ "icons/Wire 64x64f";

	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire2x2fData : brick2x2fData){
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 2x2f";
	iconName = $LuaLogic::Path @ "icons/Wire 2x2f";
	
	isLogic = true;
	isLogicWire = true;
};

datablock fxDTSBrickData(LogicWire4x4fData : brick4x4fData){
	category = "Logic Bricks";
	subCategory = "Wires";
	uiName = "Wire 4x4f";
	iconName = $LuaLogic::Path @ "icons/Wire 4x4f";
	
	isLogic = true;
	isLogicWire = true;
};
