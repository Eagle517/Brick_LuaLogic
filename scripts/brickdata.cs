
deleteVariables("$LuaLogic::BrickData::LoadedFile_*");

function lualogic_cleanfilename(%fn){
	%fn = strReplace(%fn, "\\", "/");
	%fn = strReplace(%fn, "/", "_");
	%fn = strReplace(%fn, ".", "_");
	%fn = strReplace(%fn, "-", "_");
	return %fn;
}

function lualogic_require(%fn){
	if(!$LuaLogic::BrickData::LoadedFile_[lualogic_cleanfilename(%fn)]){
		exec(%fn);
		$LuaLogic::BrickData::LoadedFile_[lualogic_cleanfilename(%fn)] = 1;
	}
}

function lualogic_execallbricks(){
	%patt = $LuaLogic::Path @ "bricks/*.cs";
	%fn = findFirstFile(%patt);
	while(%fn!$=""){
		lualogic_require(%fn);
		
		%fn = findNextFile(%patt);
	}
}

lualogic_execallbricks();

deleteVariables("$LuaLogic::BrickData::LoadedFile_*");
