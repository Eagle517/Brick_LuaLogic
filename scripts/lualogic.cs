
if($Pref::Server::LuaLogic::OPT_TICK_ENABLED $= "") $Pref::Server::LuaLogic::OPT_TICK_ENABLED = true;
if($Pref::Server::LuaLogic::OPT_TICK_MULT    $= "") $Pref::Server::LuaLogic::OPT_TICK_MULT    = 1;
if($Pref::Server::LuaLogic::OPT_TICK_TIME    $= "") $Pref::Server::LuaLogic::OPT_TICK_TIME    = 0;
if($Pref::Server::LuaLogic::OPT_FX_UPDATES   $= "") $Pref::Server::LuaLogic::OPT_FX_UPDATES   = true;
if($Pref::Server::LuaLogic::OPT_FX_TIME      $= "") $Pref::Server::LuaLogic::OPT_FX_TIME      = 0.03;

exec("./utilities.cs");
exec("./tcp.cs");
exec("./bricks.cs");
exec("./brickdata.cs");
exec("./cmds.cs");

function lualogic_loadprintsandcolors()
{
	lualogic_definecolor("RED"   , "1 0 0 1");
	lualogic_definecolor("GREEN" , "0 1 0 1");
	lualogic_definecolor("YELLOW", "1 1 0 1");
	
	lualogic_defineprint("ARROW"    , "Add-Ons/Print_Logic_Default/prints/arrow.png");
	lualogic_defineprint("UPARROW"  , "Add-Ons/Print_Logic_Default/prints/uparrow.png");
	lualogic_defineprint("DOWNARROW", "Add-Ons/Print_Logic_Default/prints/downarrow.png");
	lualogic_defineprint("ANDGATE"  , "Add-Ons/Print_Logic_Default/prints/AND.png");
	
	for(%i = 0; %i < 8; %i++)
	{
		%a = (%i >> 2) & 1;
		%b = (%i >> 1) & 1;
		%c = (%i >> 0) & 1;
		lualogic_defineprint("COLOR" @ %a @ %b @ %c, "Add-Ons/Print_Logic_Default/prints/color_" @ %a @ %b @ %c @ ".png");
	}
	
	lualogic_defineprint("space"             , "Add-Ons/Print_Letters_Default/prints/-space.png"           );
	
	lualogic_defineprint("A"                 , "Add-Ons/Print_Letters_Default/prints/A.png"                );
	lualogic_defineprint("B"                 , "Add-Ons/Print_Letters_Default/prints/B.png"                );
	lualogic_defineprint("C"                 , "Add-Ons/Print_Letters_Default/prints/C.png"                );
	lualogic_defineprint("D"                 , "Add-Ons/Print_Letters_Default/prints/D.png"                );
	lualogic_defineprint("E"                 , "Add-Ons/Print_Letters_Default/prints/E.png"                );
	lualogic_defineprint("F"                 , "Add-Ons/Print_Letters_Default/prints/F.png"                );
	lualogic_defineprint("G"                 , "Add-Ons/Print_Letters_Default/prints/G.png"                );
	lualogic_defineprint("H"                 , "Add-Ons/Print_Letters_Default/prints/H.png"                );
	lualogic_defineprint("I"                 , "Add-Ons/Print_Letters_Default/prints/I.png"                );
	lualogic_defineprint("J"                 , "Add-Ons/Print_Letters_Default/prints/J.png"                );
	lualogic_defineprint("K"                 , "Add-Ons/Print_Letters_Default/prints/K.png"                );
	lualogic_defineprint("L"                 , "Add-Ons/Print_Letters_Default/prints/L.png"                );
	lualogic_defineprint("M"                 , "Add-Ons/Print_Letters_Default/prints/M.png"                );
	lualogic_defineprint("N"                 , "Add-Ons/Print_Letters_Default/prints/N.png"                );
	lualogic_defineprint("O"                 , "Add-Ons/Print_Letters_Default/prints/O.png"                );
	lualogic_defineprint("P"                 , "Add-Ons/Print_Letters_Default/prints/P.png"                );
	lualogic_defineprint("Q"                 , "Add-Ons/Print_Letters_Default/prints/Q.png"                );
	lualogic_defineprint("R"                 , "Add-Ons/Print_Letters_Default/prints/R.png"                );
	lualogic_defineprint("S"                 , "Add-Ons/Print_Letters_Default/prints/S.png"                );
	lualogic_defineprint("T"                 , "Add-Ons/Print_Letters_Default/prints/T.png"                );
	lualogic_defineprint("U"                 , "Add-Ons/Print_Letters_Default/prints/U.png"                );
	lualogic_defineprint("V"                 , "Add-Ons/Print_Letters_Default/prints/V.png"                );
	lualogic_defineprint("W"                 , "Add-Ons/Print_Letters_Default/prints/W.png"                );
	lualogic_defineprint("X"                 , "Add-Ons/Print_Letters_Default/prints/X.png"                );
	lualogic_defineprint("Y"                 , "Add-Ons/Print_Letters_Default/prints/Y.png"                );
	lualogic_defineprint("Z"                 , "Add-Ons/Print_Letters_Default/prints/Z.png"                );
	
	lualogic_defineprint("Alcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Alcase.png"         );
	lualogic_defineprint("Blcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Blcase.png"         );
	lualogic_defineprint("Clcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Clcase.png"         );
	lualogic_defineprint("Dlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Dlcase.png"         );
	lualogic_defineprint("Elcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Elcase.png"         );
	lualogic_defineprint("Flcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Flcase.png"         );
	lualogic_defineprint("Glcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Glcase.png"         );
	lualogic_defineprint("Hlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Hlcase.png"         );
	lualogic_defineprint("Ilcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Ilcase.png"         );
	lualogic_defineprint("Jlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Jlcase.png"         );
	lualogic_defineprint("Klcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Klcase.png"         );
	lualogic_defineprint("Llcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Llcase.png"         );
	lualogic_defineprint("Mlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Mlcase.png"         );
	lualogic_defineprint("Nlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Nlcase.png"         );
	lualogic_defineprint("Olcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Olcase.png"         );
	lualogic_defineprint("Plcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Plcase.png"         );
	lualogic_defineprint("Qlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Qlcase.png"         );
	lualogic_defineprint("Rlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Rlcase.png"         );
	lualogic_defineprint("Slcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Slcase.png"         );
	lualogic_defineprint("Tlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Tlcase.png"         );
	lualogic_defineprint("Ulcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Ulcase.png"         );
	lualogic_defineprint("Vlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Vlcase.png"         );
	lualogic_defineprint("Wlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Wlcase.png"         );
	lualogic_defineprint("Xlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Xlcase.png"         );
	lualogic_defineprint("Ylcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Ylcase.png"         );
	lualogic_defineprint("Zlcase"            , "Add-Ons/Print_Letters_Lowercase/prints/Zlcase.png"         );
	
	lualogic_defineprint("0"                 , "Add-Ons/Print_Letters_Default/prints/0.png"                );
	lualogic_defineprint("1"                 , "Add-Ons/Print_Letters_Default/prints/1.png"                );
	lualogic_defineprint("2"                 , "Add-Ons/Print_Letters_Default/prints/2.png"                );
	lualogic_defineprint("3"                 , "Add-Ons/Print_Letters_Default/prints/3.png"                );
	lualogic_defineprint("4"                 , "Add-Ons/Print_Letters_Default/prints/4.png"                );
	lualogic_defineprint("5"                 , "Add-Ons/Print_Letters_Default/prints/5.png"                );
	lualogic_defineprint("6"                 , "Add-Ons/Print_Letters_Default/prints/6.png"                );
	lualogic_defineprint("7"                 , "Add-Ons/Print_Letters_Default/prints/7.png"                );
	lualogic_defineprint("8"                 , "Add-Ons/Print_Letters_Default/prints/8.png"                );
	lualogic_defineprint("9"                 , "Add-Ons/Print_Letters_Default/prints/9.png"                );
	
	lualogic_defineprint("bang"              , "Add-Ons/Print_Letters_Default/prints/-bang.png"            );
	lualogic_defineprint("at"                , "Add-Ons/Print_Letters_Default/prints/-at.png"              );
	lualogic_defineprint("pound"             , "Add-Ons/Print_Letters_Default/prints/-pound.png"           );
	lualogic_defineprint("dollar"            , "Add-Ons/Print_Letters_Default/prints/-dollar.png"          );
	lualogic_defineprint("percent"           , "Add-Ons/Print_Letters_Default/prints/-percent.png"         );
	lualogic_defineprint("caret"             , "Add-Ons/Print_Letters_Default/prints/-caret.png"           );
	lualogic_defineprint("and"               , "Add-Ons/Print_Letters_Default/prints/-and.png"             );
	lualogic_defineprint("asterisk"          , "Add-Ons/Print_Letters_Default/prints/-asterisk.png"        );
	lualogic_defineprint("minus"             , "Add-Ons/Print_Letters_Default/prints/-minus.png"           );
	lualogic_defineprint("equals"            , "Add-Ons/Print_Letters_Default/prints/-equals.png"          );
	lualogic_defineprint("plus"              , "Add-Ons/Print_Letters_Default/prints/-plus.png"            );
	lualogic_defineprint("apostrophe"        , "Add-Ons/Print_Letters_Default/prints/-apostrophe.png"      );
	lualogic_defineprint("less_than"         , "Add-Ons/Print_Letters_Default/prints/-less_than.png"       );
	lualogic_defineprint("greater_than"      , "Add-Ons/Print_Letters_Default/prints/-greater_than.png"    );
	lualogic_defineprint("period"            , "Add-Ons/Print_Letters_Default/prints/-period.png"          );
	lualogic_defineprint("qmark"             , "Add-Ons/Print_Letters_Default/prints/-qmark.png"           );
	
	lualogic_defineprint("apostrophe2"       , "Add-Ons/Print_Letters_Extra/prints/-apostrophe2.png"       );
	lualogic_defineprint("colon"             , "Add-Ons/Print_Letters_Extra/prints/-colon.png"             );
	lualogic_defineprint("comma"             , "Add-Ons/Print_Letters_Extra/prints/-comma.png"             );
	lualogic_defineprint("curlybracketleft"  , "Add-Ons/Print_Letters_Extra/prints/-curlybracketleft.png"  );
	lualogic_defineprint("curlybracketright" , "Add-Ons/Print_Letters_Extra/prints/-curlybracketright.png" );
	lualogic_defineprint("currencysign"      , "Add-Ons/Print_Letters_Extra/prints/-currencysign.png"      );
	lualogic_defineprint("euro"              , "Add-Ons/Print_Letters_Extra/prints/-euro.png"              );
	lualogic_defineprint("onehalf"           , "Add-Ons/Print_Letters_Extra/prints/-onehalf.png"           );
	lualogic_defineprint("poundsymbol"       , "Add-Ons/Print_Letters_Extra/prints/-poundsymbol.png"       );
	lualogic_defineprint("roundbracketleft"  , "Add-Ons/Print_Letters_Extra/prints/-roundbracketleft.png"  );
	lualogic_defineprint("roundbracketright" , "Add-Ons/Print_Letters_Extra/prints/-roundbracketright.png" );
	lualogic_defineprint("slashleft"         , "Add-Ons/Print_Letters_Extra/prints/-slashleft.png"         );
	lualogic_defineprint("slashright"        , "Add-Ons/Print_Letters_Extra/prints/-slashright.png"        );
	lualogic_defineprint("squarebracketleft" , "Add-Ons/Print_Letters_Extra/prints/-squarebracketleft.png" );
	lualogic_defineprint("squarebracketright", "Add-Ons/Print_Letters_Extra/prints/-squarebracketright.png");
	lualogic_defineprint("tilde"             , "Add-Ons/Print_Letters_Extra/prints/-tilde.png"             );
	lualogic_defineprint("umlaut"            , "Add-Ons/Print_Letters_Extra/prints/-umlaut.png"            );
	lualogic_defineprint("underscore"        , "Add-Ons/Print_Letters_Extra/prints/-underscore.png"        );
	lualogic_defineprint("verticalbar"       , "Add-Ons/Print_Letters_Extra/prints/-verticalbar.png"       );
	
	lualogic_defineprint("semicolon"         , "Add-Ons/Print_Letters_ExtraExtended/prints/-semicolon.png" );
	lualogic_defineprint("backtick"          , "Add-Ons/Print_Letters_ExtraExtended/prints/-backtick.png"  );
}
schedule(0, 0, "lualogic_loadprintsandcolors");

package LuaLogic
{
	function onServerDestroyed()
	{
		deleteVariables("$LuaLogic*");
		parent::onServerDestroyed();
	}
};
activatePackage("LuaLogic");

function llc(){
	lualogic_connect(25000);
}

function llr(){
	%path = $LuaLogic::Path;
	deleteVariables("$LuaLogic*");
	$LuaLogic::Path = %path;
	
	//resetAllOpCallFunc();
	exec("./lualogic.cs");
	schedule(1000, 0, llc);
}
